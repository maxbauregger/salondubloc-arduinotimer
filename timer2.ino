/*
 * This is the source code for the Arduino-Timer,
 * which is installed in "Salon Du Bloc" as a climbing stop watch
 * for Salon du Bloc Hardmoves 2013.
 *
 * License: GPL Version 3
 * © by Henry Heinemann & Max Bauregger
 */

// Array of pins for Number itself:
static const int lights[8] = {
  2, // top
  3, // top left
  4, // bottom left
  5, // top right
  6, // middle
  7, // bottom right
  8, // bottom
  9  // do
};

// Array of pins used to switch the Screen on and off
static const int digits[4] = {
  10, // left digit
  11, // middle left digit
  12, // middle right digit
  13  // right digit
};

// Main controlling variable:
// 1=running, 2=stopped, 3=stopped-win, 4=stopped-fail, -1=ready-to-start/reset, 0=useless
static int timerstate = -1; 

// These var holds the millis() value of the last 10millisec-tick.
static unsigned long lastTick = 0;

// Digits of the timer counted up
static unsigned long sec001 = 0;
static unsigned long sec01 = 0;
static unsigned int sec1 = 0;
static unsigned int sec10 = 0;
static unsigned int sec100 = 0;
static unsigned int sec1000 = 0;

// Position of the dot in the Screen
// Counted from the right side (0 equals no dot)
static unsigned int dot = 3;

// Variables holding the last state of the button
// Used for "release"-events
// All three actions the buttons used for got their own, to minimize conflict possibilities.
static boolean buttonTempStop = 0;
static boolean buttonTempStart = 0;
static boolean buttonTempReset = 0;

// Constant, how long (millisec) one digit lasts, until the next will be shown (multiplexing)
static const int screenDelay = 2;

// Constant, how long (after finish) either the time or the rating is displayed until switching
static const unsigned int blinkDur = 800; //millisec

//Variables to control the blinking finish screen
static unsigned long blinkTimer = 0;
static boolean blinkState = 0;
static int blinkCount = 0;

//Constant, which time must be underbit to win
static const int timeToWin = 6;

//Constant, how long the timer keeps maximum running
static const int endTime = 3; //*100 seconds (not higher than 9 (or change if-capsule))

// Level, what the analog inputs have to read, until an action is performed
// (There were no free digital input pins free to use)
static const int analogButtonFXLevel = 1000;

/*
 * This setup function will be called before the loop()-function
 * and shoud initialize all needed for the timer.
 */
void setup() {

  // initialize the I/O pins as outputs
  // iterate over the pins:
  for (int thisPin = 2; thisPin < 14; thisPin++) {

    // initialize the output pins:
    pinMode(thisPin, OUTPUT);

    // All Off
    digitalWrite(thisPin, LOW);
  }
}

/*
 * This method will be called as fast as possible recursively.
 * The timerstate variable decides what action is performed.
 * Warning: This method must be called as fast as possible to ensure
 * that the screen keeps displaying 4 digits (Multiplexing)
 */
void loop() {

  //Timer is Running
  if (timerstate == 1){
    readButtonsForStop(); // The timer can only be stopped out of this state.

    //Count the time up
    if (millis() - lastTick >= 10) {
      lastTick = millis();
      sec001++;
      if (sec001 >= 10){
        sec01++;
        sec001 = 0;
        if (sec01 >= 10){
          sec1++;
          sec01 = 0;
          if (sec1 >= 10){
            sec10++;
            sec1 = 0;
            if (sec10 >= 10){
              sec100++;

              // if the timer is running to long (this is the if-statement, mentioned above)
              if(sec100 >= endTime){
                timerstate=4;
              }
              sec10 = 0;

              // the screen is to small to display the full number from here.
              if(dot > 2){
                dot = 2;
              }

              // will/should never happen
              if (sec100 >= 10){
                sec1000++;
                sec100 = 0;
                if(dot > 1){
                  dot = 1;
                }
              }
            }
          }
        }
      }

      // Print the Time (Multiplexing)
      if(dot == 3){
        printNum(sec10,sec1,sec01,sec001,3,screenDelay);
      }
      else{
        if(dot == 2){
          printNum(sec100,sec10,sec1,sec01,2,screenDelay);
        }
        else{
          if(dot == 1){
            printNum(sec1000,sec100,sec10,sec1,1,screenDelay);
          }
        }
      }
    }
  }

  // Timerstate != 0 means the timer is not running
  else{ 

    // Stopped, not resetted
    if(timerstate > 1) {
      readButtonsForReset(); // The timer can only be resetted out of this state.

      // Time and word blinking
      if(millis() > (blinkTimer + (blinkDur * blinkCount))){
        blinkState = !blinkState;
        blinkCount++;
        allPinsLow();
        delay(20);
      }

      // Screen displayes the Word
      if(blinkState){

        if(timerstate == 3){ // Win
          printText(4, 2, 5, 6, screenDelay); // GEIL
        }

        if(timerstate == 4){ // Loose
          printText( 3, 1, 5, 6, screenDelay); // FAIL  
        }

        if(timerstate == 2){ // Nothing - unhandled - should not be happen
          blinkState = !blinkState;
        }

        else{} // Nothing - unhandled
      }

      // Screen displayes Time
      else{
        if(dot == 3){
          printNum(sec10,sec1,sec01,sec001,3,screenDelay);
        }
        else{
          if(dot == 2){
            printNum(sec100,sec10,sec1,sec01,2,screenDelay);
          }
          else{
            if(dot == 1){
              printNum(sec1000,sec100,sec10,sec1,1,screenDelay);
            }
          }
        }

      }
    }

    // Resetted
    else{
      readButtonsForStart(); // The timer can only be resetted out of this state.
      printText( 3, 7, 8, -1, screenDelay); // "LOS"
    }
  }
}

/*
 * This function simply compares the time with the "timeToWin"-constant
 */
void rateTime(){
  if((sec1 + (10 * sec10) + (100 * sec100) + (1000 * sec1000)) < timeToWin){
    timerstate = 3;
  }
  else {
    timerstate = 4;
  }
}

/*
 * Method to stop the timer when a button is pressed
 */
void readButtonsForStop(){

  //Button pressed at the top!!
  if (analogRead(1) > analogButtonFXLevel) {stopTimer();}

  // Button pressed at the start -> wait for release to stop
  if(analogRead(0) > analogButtonFXLevel){buttonTempStop = 1;}

  // Button at the bottom not (anymore) pressed
  else{

    // Button released
    if(buttonTempStop == 1){
      buttonTempStop = 0; // reset release
      stopTimer();
    }
  }
}

/*
 * Method to start the timer when the start button is pressed
 */
void readButtonsForStart(){

  // Button pressed at the start -> wait for release to start
  if(analogRead(0) > analogButtonFXLevel){
    buttonTempStart = 1;
  }

  // Button at the start not (anymore) pressed
  else{

    // Button released
    if(buttonTempStart){
      buttonTempStart = 0; // reset release
      timerstate = 1;
    }
  }
}

/*
 * Method to start the timer when the start button is pressed
 */
void readButtonsForReset(){

  // Button pressed at the start -> wait for release to reset
  if(analogRead(0) > analogButtonFXLevel){
    buttonTempReset = 1;
  }

  // Button at the start not (anymore) pressed
  else{

    // Button released
    if(buttonTempReset){
      buttonTempReset = 0; // reset release
      resetTimer();
    }
  }
}

/*
 * This function actually sets the variables to stop the timer.
 */
void stopTimer(){
  timerstate = 2;      
  rateTime();
  blinkTimer = millis();
}

/*
 * This function actually sets the variables to reset the timer.
 */
void resetTimer(){
  timerstate = -1;
  sec001 = 0;
  sec01 = 0;
  sec1 = 0;
  sec10 = 0;
  sec100 = 0;
  sec1000 = 0;
  dot = 3;

  blinkTimer = 0;
  blinkState = 0;
  blinkCount = 0;
}

/*
 * Main method to print a value on the screen.
 * param int digit1000, Number for left digit
 *       int digit100,  Number for middle left digit
 *       int digit10,   Number for middle right digit
 *       int digit1,    Number for left digit
 *       int dot,       Position of the dot (See dot var declaration)
 *       int delaytime  Time a single digit is displayed (Multiplexing)
 */
void printNum(int digit1000, int digit100, int digit10, int digit1, int dot, int delaytime){

  allPinsLow();
  if (dot == 4){
    buildNumeral(digit1000, 1);
  }
  else{
    buildNumeral(digit1000, 0);
  }
  digitalWrite(digits[3], HIGH);
  delay(delaytime);

  allPinsLow();
  if (dot == 3){
    buildNumeral(digit100, 1);
  }
  else{
    buildNumeral(digit100, 0);
  }
  digitalWrite(digits[2], HIGH);
  delay(delaytime);

  allPinsLow();
  if (dot == 2){
    buildNumeral(digit10, 1);
  }
  else{
    buildNumeral(digit10, 0);
  }
  digitalWrite(digits[1], HIGH);
  delay(delaytime);

  allPinsLow();
  if (dot == 1){
    buildNumeral(digit1, 1);
  }
  else{
    buildNumeral(digit1, 0);
  }
  digitalWrite(digits[0], HIGH);
  delay(delaytime);
}

void printText(int char1, int char2, int char3, int char4, int delaytime){

  allPinsLow();
  buildChar(char1);
  digitalWrite(digits[3], HIGH);
  delay(delaytime);

  allPinsLow();
  buildChar(char2);
  digitalWrite(digits[2], HIGH);
  delay(delaytime);

  allPinsLow();
  buildChar(char3);
  digitalWrite(digits[1], HIGH);
  delay(delaytime);

  allPinsLow();
  buildChar(char4);
  digitalWrite(digits[0], HIGH);
  delay(delaytime);
}

void buildNumeral(int number, boolean dot){
  switch(number){
    case 0:
      buildZero();
      break;
    case 1:
      buildOne();
      break;
    case 2:
      buildTwo();
      break;
    case 3:
      buildThree();
      break;
    case 4:
      buildFour();
      break;
    case 5:
      buildFive();
      break;
    case 6:
      buildSix();
      break;
    case 7:
      buildSeven();
      break;
    case 8:
      buildEight();
      break;
    case 9:
      buildNine();
      break;
    case -1:
      buildNone();
      break;
  }

  if(dot){
    digitalWrite(lights[7], HIGH);
  }
  else{
    digitalWrite(lights[7], LOW);
  }
}

void buildChar(int letter){
  switch(letter){
    case -1:       //Empty char
      buildNone();
      break;
    case 1:        //LETTER A
      buildA();
      break;
    case 2:        //LETTER E
      buildE();
      break;
    case 3:        //LETTER F
      buildF();
      break;
    case 4:        //LETTER G
      buildG();
      break;
    case 5:        //LETTER I
      buildOne();
      break;
    case 6:        //LETTER L
      buildL();
      break;
    case 7:        //LETTER O
      buildZero();
      break;
    case 8:        //LETTER S
      buildFive();
      break;
    case 9:        //EXCLAMATION MARK
      buildExclamation();
      break;
  }
}

void buildOne(){
  digitalWrite(lights[3], HIGH);
  digitalWrite(lights[5], HIGH);
}

void buildTwo(){
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[2], HIGH);
  digitalWrite(lights[3], HIGH);
  digitalWrite(lights[4], HIGH);
  digitalWrite(lights[6], HIGH);
}

void buildThree(){
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[3], HIGH);
  digitalWrite(lights[4], HIGH);
  digitalWrite(lights[5], HIGH);
  digitalWrite(lights[6], HIGH);
}

void buildFour(){
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[3], HIGH);
  digitalWrite(lights[4], HIGH);
  digitalWrite(lights[5], HIGH);
}

void buildFive(){
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[4], HIGH);
  digitalWrite(lights[5], HIGH);
  digitalWrite(lights[6], HIGH);
}

void buildSix(){
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[2], HIGH);
  digitalWrite(lights[4], HIGH);
  digitalWrite(lights[5], HIGH);
  digitalWrite(lights[6], HIGH);
}

void buildSeven(){
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[3], HIGH);
  digitalWrite(lights[5], HIGH);
}

void buildEight(){
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[2], HIGH);
  digitalWrite(lights[3], HIGH);
  digitalWrite(lights[4], HIGH);
  digitalWrite(lights[5], HIGH);
  digitalWrite(lights[6], HIGH);
}

void buildNine(){
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[3], HIGH);
  digitalWrite(lights[4], HIGH);
  digitalWrite(lights[5], HIGH);
  digitalWrite(lights[6], HIGH);
}

void buildZero(){
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[2], HIGH);
  digitalWrite(lights[3], HIGH);
  digitalWrite(lights[5], HIGH);
  digitalWrite(lights[6], HIGH);
}

void buildL(){
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[2], HIGH);
  digitalWrite(lights[6], HIGH);
}

void buildF(){
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[2], HIGH);
  digitalWrite(lights[4], HIGH);
}

void buildA(){
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[2], HIGH);
  digitalWrite(lights[3], HIGH);
  digitalWrite(lights[4], HIGH);
  digitalWrite(lights[5], HIGH);
}

void buildE(){
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[2], HIGH);
  digitalWrite(lights[4], HIGH);
  digitalWrite(lights[6], HIGH);
}

void buildG(){
  digitalWrite(lights[0], HIGH);
  digitalWrite(lights[1], HIGH);
  digitalWrite(lights[2], HIGH);
  digitalWrite(lights[5], HIGH);
  digitalWrite(lights[6], HIGH);
}

void buildExclamation(){ // exclamation mark
  digitalWrite(lights[3], HIGH);
  digitalWrite(lights[5], HIGH);
  digitalWrite(lights[7], HIGH);
}

void buildNone(){
  //none
}

void allPinsLow(){
  digitalWrite(lights[0], LOW);
  digitalWrite(lights[1], LOW);
  digitalWrite(lights[2], LOW);
  digitalWrite(lights[3], LOW);
  digitalWrite(lights[4], LOW);
  digitalWrite(lights[5], LOW);
  digitalWrite(lights[6], LOW);
  digitalWrite(lights[7], LOW);
  digitalWrite(digits[0], LOW);
  digitalWrite(digits[1], LOW);
  digitalWrite(digits[2], LOW);
  digitalWrite(digits[3], LOW);
}